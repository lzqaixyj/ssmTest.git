package manage.controller;

import manage.model.User;
import manage.service.UserService;
import manage.service.impl.UserServiceImpl;
import manage.utils.ConstantString;
import manage.utils.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "/manage/user/")
public class UserController {
    @Autowired
    private UserService us;
    @ResponseBody
    @RequestMapping(value = "select",method = RequestMethod.GET)
    public JsonResult select(){
        JsonResult result;
        List<User> list=us.findUser();
        try {
            if (list.size()>0) {
                result=new JsonResult("查询成功",ConstantString.STATUS_SUCCESS,list);
            }else {
                result=new JsonResult("查询失败",ConstantString.STATUS_ERRO);
            }
        }catch (Exception e){
            result=new JsonResult("查询异常",ConstantString.STATUS_EXCEPTION);
        }
        return result;
    }
}
