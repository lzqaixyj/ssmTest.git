package manage.dao;

import manage.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
@Repository
public interface UserDao {
    public User login(Map<String , Object> map);
    public List<User> select();
}
