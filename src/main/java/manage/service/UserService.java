package manage.service;

import manage.model.User;

import java.util.List;

public interface UserService {
    public List<User> findUser();
}
