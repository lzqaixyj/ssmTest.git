package manage.service.impl;

import manage.dao.UserDao;
import manage.model.User;
import manage.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao dao;
    @Override
    public List<User> findUser() {
        return dao.select();
    }
}
