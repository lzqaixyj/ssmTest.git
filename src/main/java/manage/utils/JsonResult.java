package manage.utils;

public class JsonResult<T> {
    private String message;
    private String status;
    private T object;
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public T getObject() {
        return object;
    }

    public void setObject(T object) {
        this.object = object;
    }


    public JsonResult(String message,String status,T object){
        this.message=message;
        this.status=status;
        this.object=object;
    }
    public JsonResult(String message,String status){
        this.message=message;
        this.status=status;
    }
}
