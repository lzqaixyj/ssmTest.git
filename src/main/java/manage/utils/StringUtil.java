package manage.utils;

public class StringUtil {
    /**
     *将对象转换为字符串，如果为null就为null
     * @param object
     * @return
     */
    public static String valueof(Object object){
        return (object == null)?null:object.toString();
    }

    /**
     * 将对象转换为字符串，如果为null就转为空串
     * @param object
     * @return
     */
    public static String emptyof(Object object){
        return (object == null)?"":object.toString();
    }
}
